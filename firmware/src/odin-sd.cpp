/*
   TODO Implement comms throuhg HW serial to GSM module (https://github.com/vshymanskyy/TinyGSM)
   TODO Set clock by cell phone information
 */
#include <Arduino.h>

/* ODIN-SD firmware for ATMega*/
// SD card libraries
#include <SPI.h>
#include <SD.h>
//Chronodot libraries
#include <RTClib.h>
#include <Wire.h>

//Advanced power management libraries
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/io.h>
//Software Serial for sensors
#include <SoftwareSerial.h>
//T&RH SHT31 sensor library
#include <Adafruit_SHT31.h>

//GSM library for MQTT client
#define TINY_GSM_MODEM_SIM800

#include <TinyGsmClient.h>
#include <PubSubClient.h>

// functions
String timestring(DateTime time);
void RTC_send_register(byte reg, byte value);
void sleepNow(void);
void pinInterrupt(void);
void readDust(void);
void GetNO2(void);
// void GetCO(void);

// Constant definitions
//PMS3003 definitions
#define receiveDatIndex 24 // Sensor data payload size
#define dustPin 4 // Pin for the "sleep" trigger for dust sensor
#define dustRX 8 // Pin to receive data from dust sensor
#define dustTX 9 // Pin to send data to dust sensor NOT CONNECTED

//NO2 definitions
#define no2RX 4 // Pin to receive data from dust sensor
#define no2TX 2 // Pin to send data to dust sensor NOT CONNECTED

//CO definitions
#define coRX A0 // Pin to receive data from dust sensor
#define coTX A1 // Pin to send data to dust sensor NOT CONNECTED

// Indicator light
#define RedPin 9 // Pin where the light indicator is located
#define GreenPin 3 // Pin where the light indicator is located
#define YellowPin 5 // Pin where the light indicator is located

//Clock definition
#define clockPin 3 // Pin for the interrupt from the RTC

// Modem definitions
// Your GPRS credentials
// Leave empty, if missing user or pass
char apn[15]  = "YourAPN";

TinyGsm modem(Serial);
TinyGsmClient client(modem);
PubSubClient mqtt(client);
char broker[25] = "test.mosquitto.org";

char topicLed[25] = "GsmClientTest/status";
char topicInit[25] = "GsmClientTest/data";
//Variable declarations
byte receiveDat[receiveDatIndex]; //receive data from the air detector module
byte readbuffer[64];
unsigned int checkSum,checkresult;
unsigned int FrameLength,Data4,Data5,Data6;
unsigned int PM1,PM25,PM10,N300,N500,N1000,N2500,N5000,N10000;
int length;
unsigned long timer;
float t,rh;
bool valid_data,iamok;
// File object
File myFile;
// RTC object
RTC_DS3231 RTC;
DateTime curr_time,prev_time;
// Set time and date for when the clock has no time
char date_in[] = "Oct 21 2015"; // Yes, it's THAT date
char time_in[] = "07:28:00";
String sdCard,fname,NO2,CO;
char file_fname[20];
int firstloop = 1;
// SHT31-D object
Adafruit_SHT31 sht31 = Adafruit_SHT31();

//SoftwareSerial rxPIN, txPIN
SoftwareSerial dustport(dustRX,dustTX);
SoftwareSerial no2port(no2RX,no2TX);
// SoftwareSerial coport(coRX,coTX);


void setup() {
								iamok = true;
								//set the HW serial port (Modem)
								Serial.begin(115200);
                delay(3000);
                modem.init();
								//set the serial's Baudrate of the air detector module
								dustport.begin(9600);
								//set the serial's Baudrate of the NO2 module
								no2port.begin(9600);
								//set the serial's Baudrate of the CO module
								// coport.begin(9600);
								pinMode(RedPin,OUTPUT);
								pinMode(GreenPin,OUTPUT);
								digitalWrite(RedPin,LOW);
								digitalWrite(GreenPin,LOW);
								Serial.println(F("Starting SHT31-D"));
								sht31.begin();
								Serial.println(F("Starting RTC"));
								RTC.begin();
								if (RTC.lostPower()) {
																iamok = false;
																Serial.println(F("RTC is NOT running! Setting time to BTTF time"));
																// following line sets the RTC to the date & time this sketch was compiled
																RTC.adjust(DateTime(date_in, time_in));
																int j = 1;
																while (j < 20) {
																								digitalWrite(RedPin,LOW);
																								delay(100);
																								digitalWrite(RedPin,HIGH);
																								delay(100);
																								j = j+1;
																}
								}
								else {
																Serial.println("RTC running OK");
								}
								prev_time = RTC.now();
								Serial.println(timestring(prev_time));
								Serial.println(F("Initializing SD card..."));

								// On the Ethernet Shield, CS is pin 4. It's set as an output by default.
								// Note that even if it's not used as the CS pin, the hardware SS pin
								// (10 on most Arduino boards, 53 on the Mega) must be left as an output
								// or the SD library functions will not work.
								pinMode(10, OUTPUT);
								if (!SD.begin(10)) {
																Serial.println(F("initialization failed!"));
																iamok = false;
																digitalWrite(YellowPin,HIGH);
																return;
								}
								Serial.println(F("initialization done."));
								fname=String(String(sdCard)+".txt");
								fname.toCharArray(file_fname,fname.length()+1); // Convert the string to char
								myFile = SD.open(file_fname, FILE_WRITE);
								myFile.println(F("Day\tTime\tFrameLength\tPM1\tPM2.5\tPM10\tData4\tData5\tData6\tData7\tData8\tData9\tTemperature\tRH"));
								myFile.close();

								pinMode(dustPin,INPUT); //To switch the dust module on/off
								digitalWrite(dustPin,HIGH);
								// Set the alarm to go off at each minute
								// Write to the 7,8,9 and A registers on Alarm1
								RTC_send_register(0x07,B00000000);
								RTC_send_register(0x08,B10000000);
								RTC_send_register(0x09,B10000000);
								RTC_send_register(0x0A,B10000000);
								// This will enable Alarm and also enables the alarm interrupt to be generated on the SQW pin
								RTC_send_register(0x0E,B00000101);
								RTC_send_register(0x0F,0); // 28_08_2014 Discovered that it is important to reset SQW interrupt when power up ODIN
								pinMode(clockPin,INPUT);
}
String timestring(DateTime time){
								// This function gets the data from the Chronodot and converts to strings.
								String xmonth, xday, xhour, xminute, xsecond;
								// For one digit months
								if (time.month()<10) {
																xmonth="0"+String(time.month());
								}
								else {
																xmonth=String(time.month());
								}
								//One digit days
								if (time.day()<10) {
																xday="0"+String(time.day());
								}
								else {
																xday=String(time.day());
								}
								//For one digit hours
								if (time.hour()<10) {
																xhour="0"+String(time.hour());
								}
								else {
																xhour=String(time.hour());
								}
								//One digit minutes
								if (time.minute()<10) {
																xminute="0"+String(time.minute());
								}
								else {
																xminute=String(time.minute());
								}
								//One digit seconds
								if (time.second()<10) {
																xsecond="0"+String(time.second());
								}
								else {
																xsecond=String(time.second());
								}
								// xx2 gives date and time when sensor data collected.
								String xx=String(time.year())+"/"+xmonth+"/"+xday;
								String xx2= xx+"\t"+xhour+":"+xminute+":"+xsecond;
								// Conversion of the month and date to a string which will be displayed as the sdCard file name
								sdCard =String(time.year())+xmonth+xday;
								return xx2;
}
void RTC_send_register(byte reg, byte value){
								// This function writes and read data from the chronodot registers
								Wire.beginTransmission(DS3231_ADDRESS);
								Wire.write(reg);
								Wire.write(value);
								Wire.endTransmission();
}
void sleepNow(void) {
								// Put the dust sensor to sleep for a few seconds
								pinMode(dustPin,OUTPUT);
								digitalWrite(dustPin,LOW);
								Serial.println(F("I sent the dust sensor to sleep"));

								// Set pin 3 as interrupt and attach handler:
								attachInterrupt(1, pinInterrupt, LOW); // Interrupt on digital pin 3.
								delay(100);
								Serial.println(F("I attached the interrupt"));
								//
								// Choose our preferred sleep mode:
								set_sleep_mode(SLEEP_MODE_PWR_DOWN);// lowest power mode
								Serial.println(F("I set the sleep mode"));
								//
								// Set sleep enable (SE) bit:
								sleep_enable();
								Serial.println(F("I enabled the SE bit"));
								//
								// Put the device to sleep:
								sleep_mode();
								Serial.println(F("I'm awake!"));
								delay(100);
								//
								// Upon waking up, sketch continues from this point.
								RTC_send_register(0x0F,0);// Cancel alarm flag so interrupt not reasserted
								Serial.println(F("I'll wake the sensor up now"));
								pinMode(dustPin,INPUT);
								digitalWrite(dustPin,HIGH);
}
void pinInterrupt(void){
								sleep_disable();
								detachInterrupt(1);
}
void readDust(void){
								while (dustport.peek()!=66) {
																receiveDat[0]=dustport.read();
								}
								dustport.readBytes((char *)receiveDat,receiveDatIndex);
								checkSum = 0;
								for (int i = 0; i < receiveDatIndex; i++) {
																checkSum = checkSum + receiveDat[i];
								}
								checkresult = receiveDat[receiveDatIndex-2]*256+receiveDat[receiveDatIndex-1]+receiveDat[receiveDatIndex-2]+receiveDat[receiveDatIndex-1];
								valid_data = (checkSum == checkresult);
}
void GetNO2(void){
	while (!no2port.available()){
		delay(1);
	}
	char delim=',';
	String serial_no2 = no2port.readStringUntil(delim);
	NO2 = no2port.readStringUntil(delim);
}
// void GetCO(void){
// 	while (!coport.available()){
// 		delay(1);
// 	}
// 	char delim=',';
// 	String serial_co = coport.readStringUntil(delim);
// 	CO = coport.readStringUntil(delim);
// }

void loop() {
								// Clear the serial receive buffer
								dustport.readBytes((char *)readbuffer,64);
								curr_time = RTC.now();
								int nrecs = 0;
								digitalWrite(GreenPin,HIGH);
								while ((!valid_data)) {
																//Waiting for something at the serial port
																readDust();
																delay(5);
																nrecs = nrecs +1;
								}
								GetNO2();
								// GetCO();
								t = sht31.readTemperature();
								rh = sht31.readHumidity();
								digitalWrite(GreenPin,LOW);
								if (valid_data) {
																prev_time = curr_time;
																Serial.print(F("Valid data? "));
																Serial.println(valid_data);
																String timestamp = timestring(curr_time);
																FrameLength = (receiveDat[2]*256)+receiveDat[3];
																PM1 = (receiveDat[4]*256)+receiveDat[5];
																PM25 = (receiveDat[6]*256)+receiveDat[7];
																PM10 = (receiveDat[8]*256)+receiveDat[9];
																Data4 = (receiveDat[10]*256)+receiveDat[11];
																Data5 = (receiveDat[12]*256)+receiveDat[13];
																Data6 = (receiveDat[14]*256)+receiveDat[15];
																N300 = (receiveDat[16]*256)+receiveDat[17];
																N500 = (receiveDat[18]*256)+receiveDat[19];
																N1000 = (receiveDat[20]*256)+receiveDat[21];
																Serial.print(timestamp);
																Serial.print(F(" PM2.5 = "));
																Serial.println(PM25);
																Serial.print(F(" NO2 = "));
																Serial.println(NO2);
																Serial.print(F(" CO = "));
																Serial.println(CO);
																Serial.print(F(" Temperature = "));
																Serial.println(t);
																Serial.print(F(" RH = "));
																Serial.println(rh);
																fname=String(String(sdCard)+".txt");
																fname.toCharArray(file_fname,fname.length()+1); // Convert the string to char
																myFile = SD.open(file_fname, FILE_WRITE);
																if (myFile) {
																								myFile.print(timestamp);
																								myFile.print(F("\t"));
																								myFile.print(FrameLength);
																								myFile.print(F("\t"));
																								myFile.print(PM1);
																								myFile.print(F("\t"));
																								myFile.print(PM25);
																								myFile.print(F("\t"));
																								myFile.print(PM10);
																								myFile.print(F("\t"));
																								myFile.print(Data4);
																								myFile.print(F("\t"));
																								myFile.print(Data5);
																								myFile.print(F("\t"));
																								myFile.print(Data6);
																								myFile.print(F("\t"));
																								myFile.print(N300);
																								myFile.print(F("\t"));
																								myFile.print(N500);
																								myFile.print(F("\t"));
																								myFile.print(N1000);
																								myFile.print(F("\t"));
																								myFile.print(NO2);
																								myFile.print(F("\t"));
																								myFile.print(CO);
																								myFile.print(F("\t"));
																								myFile.print(t);
																								myFile.print(F("\t"));
																								myFile.print(rh);
																								myFile.close();
																								Serial.println(F("Data recorded"));
																}
																else {
																								int j = 1;
																								while (j < 20) {
																																digitalWrite(RedPin,LOW);
																																delay(100);
																																digitalWrite(RedPin,HIGH);
																																delay(100);
																																j = j+1;
																								}
																}
								}
								else {
								}
								valid_data = 1==0;
								// Need to cancel interrupt flag if it occurred during start-up routine before putting to sleep
								if (firstloop == 1) {
																RTC_send_register(0x0F,0);
																firstloop = 0;
								}
								sleepNow();
}
