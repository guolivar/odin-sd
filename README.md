# Outdoor Dust Information Node - Size Distribution version (ODIN-SD) #

Here you will find the electronic design files as well as the firmware required to build and program the ODIN-SD sensor.
The firmware is intended for an ATMega328p microcontroller (Arduino pro mini) and the electronic design was done using KiCAD

### How do I use this? ###

First you'll need the hardware for the sensor. The KiCAD design files have all the information needed to build the board but you'll need these external components:

* DHT22 Temperature and RH sensor
* PMS3003 Dust sensor
* Adafruit's microSD card reader breakout board
* Sparkfun's Arduino pro Mini

The firmware file states the required libraries.

### Do you want to help? ###

Please test the code and submit any and all bugs/wishes to this repository.

## License ##
This software is licensed under the GNU General Public License v3. A quick summary can be:

*You may copy, distribute and modify the software as long as you track changes/dates in source files. Any modifications to or software including (via compiler) GPL-licensed code must also be made available under the GPL along with build & install instructions.*

You can find the full text of the license in the repository.